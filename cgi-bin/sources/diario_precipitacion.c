#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAM 200

void diario_precipitacion( char* nro_estacion )
{
	gen_acum_diario_prec( nro_estacion );

	char linea[ TAM ];
	memset( linea ,'\0', sizeof( linea ));

	char nro_estacion_copia[ TAM ];
	memset( nro_estacion_copia, '\0', sizeof( nro_estacion_copia ));

	strcpy( nro_estacion_copia, nro_estacion );
	FILE* acumulado_diario = fopen( strcat( nro_estacion_copia, "_acum_diario" ), "r" );

	while( fgets( linea, TAM,( FILE* )acumulado_diario ) != NULL)
	{
		char* parseo = strtok( linea, "\n" );
		printf( "<p>%s</p>\n", parseo );
	}
}

