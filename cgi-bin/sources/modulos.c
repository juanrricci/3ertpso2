#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAM 256

int main ( )
{
	char linea [ TAM ];
	memset ( linea, '\0', TAM );

        char copia_linea [ TAM ];
        memset ( copia_linea, '\0', TAM );

	int contador_linea = 1;
	char* parseo;

	encabezado ( );
	printf ( "<h1>MÓDULOS DEL SISTEMA</h1>\n" );

//	printf( "<p>Instalacion de modulo</p>\n" );
//      system ( "sudo insmod /var/www/html/cgi-bin/modulo.ko" );

	FILE* retorno = popen ( "lsmod", "r" );

	while ( fgets ( linea, TAM, ( FILE* ) retorno ) != NULL )
	{
		if ( contador_linea == 2 ) 
		{
			strcpy ( copia_linea, linea );
			parseo = strtok ( copia_linea, " " );
//                      printf ( "<p>%s</p>\n", parseo );
	                printf ( "<p>%s</p>\n", linea );
		}

		else
		{
			printf ( "<p>%s</p>\n", linea );
		}
		
		contador_linea++;
	}

	pclose ( retorno );

	// Se ejecuta el comando "ls | grep modulo.ko" para saber si ya hay un módulo subido

	retorno = popen ( "ls | grep modulo.ko", "r" );

        if ( !strcmp ( parseo, "hello" ))
        {
//              printf ( "<p>Modulo INSTALADO</p>\n" );
                printf ( "<form action=\"/cgi-bin/desinstalar_modulo.cgi\">\n" );
                        printf ( "<input type=\"submit\" value=\"Desinstalar modulo HELLO\">\n" );
                printf ( "</form>\n" );
        }


        else if ( fgets ( linea, TAM, ( FILE* ) retorno ) != NULL )
        {
                printf ( "<p>Modulo subido, elegir acción a realizar:</p>\n" );
	        printf( "<form action=\"/cgi-bin/modulo_subido.cgi\" method=\"get\">\n"  );
  			printf ( "<input type=\"radio\" name=\"opcion\" value=\"instalar\" checked> Instalar<br>\n" );
  			printf ( "<input type=\"radio\" name=\"opcion\" value=\"eliminar\"> Eliminar<br>\n" );
                        printf ( "<input type=\"submit\" value=\"Aceptar\">\n" );
		printf ( "</form>\n" );
        }
	
	else
	{
	       // Formulario para subir módulo compilado

        	printf ( "<form action=\"/cgi-bin/module_uploader.cgi\" enctype=\"multipart/form-data\" method=\"post\">\n" );
                	printf ( "<p>\n" );
                        	printf ( "Subir e instalar módulo de kernel<br>\n" );
                        	printf ( "<input type=\"file\" name=\"datafile\" accept=\".ko\">\n" );
                	printf ( "</p>\n" );
                	printf ( "<div>\n" );
                        	printf ( "<input type=\"submit\" value=\"Subir\">\n" );
	                printf ( "</div>\n" );
        	printf ( "</form> \n" );
	}
        
        pclose ( retorno );	

	pie ( );
	
	return 0;
}

