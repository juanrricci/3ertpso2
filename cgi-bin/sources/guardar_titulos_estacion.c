#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAM 200

void guardar_titulos_estacion( char* nro_estacion )
{
	char linea[ TAM ];
    memset( linea, '\0', sizeof( linea ));

    char dato[ TAM ];
    memset( dato, '\0', sizeof( dato ));

    char titulo_variable[ TAM ];
	memset( titulo_variable, '\0', sizeof( titulo_variable ));    

   	int contador_datos = 1;
   	char* parseo;

	// Apertura del archivo para lectura de los datos de la estación.
	FILE* datos_estacion = fopen( nro_estacion, "r" );

	// Apertura del archivo para lectura de los títulos de las variables.
	FILE* nombres_variables = fopen( "titulos", "r" );

	// Generación de archivo donde se almacenarán los títulos de las variables
	// de la estación.
	FILE* titulos_estacion = fopen( strcat( nro_estacion, "_titulos" ), "w" );

	// Se extrae una línea de datos de la estación.
 	fgets( linea, TAM,( FILE* )datos_estacion );
 	// Se obtiene el primer dato de la línea extraída.
 	parseo = strtok( linea, "," );
 	// Se copia al string "dato".
 	strcpy( dato, parseo );

 	// Se recorre la línea de datos hasta encontrar el caracter de
 	// salto de línea.
	while( strcmp( dato, "\n" ))
	{
		contador_datos++;
		parseo = strtok( NULL, "," );
		strcpy( dato, parseo );			
		
		// Se chequea que el dato contenga un valor válido y que sea
		// desde el sexto dato en adelante (que son los datos de variables
		// de interés).
		if( strcmp( dato, "--" ) && contador_datos > 5 )
		{
			// Se extrae el nombre de la variable del archivo de títulos.
			fgets( titulo_variable, TAM, (FILE* )nombres_variables );
			// Se guarda el nombre de la variable en el archivo de títulos
			// específico de esa estación.
			fputs( titulo_variable, titulos_estacion );
			// Se limpia el string.
			memset( titulo_variable, '\0', sizeof( titulo_variable ));    
		}

		else
		{
			// Se extrae el nombre de la variable del archivo de títulos para
			// no perder el orden de los títulos de las variables, pero no
			// se almacena este nombre.
			fgets( titulo_variable, TAM, (FILE* )nombres_variables );
			memset( titulo_variable, '\0', sizeof( titulo_variable ));    
		}			
	}

	// Se cierran todos los archivos utilizados.
	fclose(titulos_estacion);
	fclose(datos_estacion);
	fclose(nombres_variables);
}
