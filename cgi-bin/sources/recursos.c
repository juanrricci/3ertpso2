#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main( )
{
	encabezado( );
	printf( "<h1>RECURSOS DEL SISTEMA</h1>\n" );
	procesador( );
	memoria( );
	uptime( );
	fechahora( );
	pie( );
	
	return 0;
}

