#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void guardar_titulos(char* linea)
{
  int contador_titulos = 0;
  char* titulo; //< Puntero para almacenar el string parseado por "strtok".
  FILE* titulos = fopen( "titulos", "w" ); //< Archivo donde se guardaran
                                           //  los títulos de las variables.

  titulo = strtok( linea, "," ); //< Parseo del primer nombre encontrado.

  while( strcmp( titulo, "\n" )) //< Bucle de parseo. Finaliza cuando encuentra
                                 //  el caracter de fin de línea.
  {
    if( contador_titulos > 0 ) //< Contador que evita colocar un salto de linea
                               //  antes de la primera línea del archivo.
    {
      fputc( '\n', titulos);
    }
    fputs( titulo, titulos );
  	titulo = strtok( NULL, ","); //< Mismo procedimiento para los demás nombres.
    contador_titulos++;
  }

  fclose( titulos ); //< Se cierra el archivo "títulos".
}
