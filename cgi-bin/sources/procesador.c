#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAM 256

void procesador( )
{
        char linea[ TAM ];
        memset( linea, '\0', TAM );

	printf( "<h2>PROCESADOR</h2>\n" );
        FILE* archivo = fopen( "/proc/cpuinfo", "r" );
        while( fgets( linea, TAM, ( FILE* ) archivo ) != NULL )
        {
                printf( "<p>%s</p>", linea );
        }

        fclose( archivo );
}
