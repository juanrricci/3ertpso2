#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void encabezado( )
{
	printf( "<!doctype html>\n" );
	printf( "\n" );
	printf( "<html lang=\"es-ar\">\n" );
		printf( "<head>\n" );
			printf( "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" );
			printf( "<title>SMN</title>\n" );
			printf( "<meta name=\"description\" content=\"SMN\">\n" );
			printf( "<meta name=\"author\" content=\"Juan Ricci\">\n" );
			printf( "\n" );
			printf( "<link rel=\"stylesheet\" href=\"css/styles.css?v=1.0\">\n" );
		printf( "</head>\n" );
		printf( "\n" );
		printf( "<body>\n" );
			printf( "<script src=\"js/scripts.js\"></script>\n" );
			printf( "<h2>Servicio Meteorológico Nacional</h2>\n" );
			printf( "<h1>Sistema hidro-meteorológico</h1>\n" );
			printf( "<a href = \"/\">Volver al inicio</a>\n" );
}
