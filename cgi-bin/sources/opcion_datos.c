#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAM 256

int main( )
{
	char get_copia[ TAM ];
	memset( get_copia, '\0', TAM );

	parseo_general( );

	char* get = getenv( "QUERY_STRING" );
	strcpy(get_copia, get);

	char* parseo = strtok( get_copia, "=" );

	encabezado( );

	if( !strcmp( parseo, "descargar" ))
	{
		parseo = strtok( NULL, "\n" );
		printf( "<p><a href=\"/download/%s\" download>Descargar</a></p>\n", parseo );
	
		//printf( "<p><a href=\"/download/pepe.txt\" download=\"pepe.txt\">Descargar</a></p>\n" );
	}
	else if( !strcmp( parseo, "diario_precipitacion" ))
	{
		parseo = strtok( NULL, "\n" );
		diario_precipitacion( parseo );
	}
	else if( !strcmp( parseo, "mensual_precipitacion" ))
	{
		parseo = strtok( NULL, "\n" );
		mensual_precipitacion( parseo );
	}
	else if( !strcmp( parseo, "promedio" ))
	{
		parseo = strtok( NULL, "\n" );
		promedio_variable( parseo );
	}


	//FILE* retorno = popen( "lsmod", "r" );
	//while( fgets( linea, TAM, ( FILE* ) retorno ) != NULL )
	//{
	//	printf( "<p>%s</p>\n", linea );
	//}
	//pclose( retorno );
	
	pie( );
	
	return 0;
}

