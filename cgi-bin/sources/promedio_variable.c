#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAM 200

void promedio_variable( char* titulo )
{
	char nro_estacion[ TAM ];
    memset( nro_estacion, '\0', sizeof( nro_estacion ));

    char* nro_estacion_parseado;

    // Apertura del archivo con los números de estaciones.
	FILE* nros_estaciones = fopen( "nros_estaciones", "r" );

	// Bucle que extrae cada número de estación.
	while( fgets( nro_estacion, TAM, ( FILE* )nros_estaciones) != NULL )
	{
		nro_estacion_parseado = strtok( nro_estacion, "\n" );
		// Llamada a la función que guarda los títulos de las variables
		// de cada estación en archivos separados.
		guardar_titulos_estacion( nro_estacion_parseado );
    	memset( nro_estacion, '\0', sizeof( nro_estacion ));
	}

	rewind( nros_estaciones );

	// Bucle que extrae cada número de estación.
	while( fgets( nro_estacion, TAM, ( FILE* )nros_estaciones) != NULL )
	{
		nro_estacion_parseado = strtok( nro_estacion, "\n" );
		// Llamada a la función que calcula los promedios de todas las 
		// variables de cada estacion.
		guardar_promedios_estacion( nro_estacion_parseado );
    	memset( nro_estacion, '\0', sizeof( nro_estacion ));
	}

	rewind( nros_estaciones );
	
	// Bucle que extrae cada número de estación.
	while( fgets( nro_estacion, TAM, ( FILE* )nros_estaciones) != NULL )
	{
		nro_estacion_parseado = strtok( nro_estacion, "\n" );
		// Llamada a la función que concatena los promedios de todas las 
		// variables de cada estacion, con sus respectivos nombres.
		concat_titulo_promedio( nro_estacion_parseado );
    	memset( nro_estacion, '\0', sizeof( nro_estacion ));
	}	

	rewind( nros_estaciones );
	
	// Bucle que extrae cada número de estación.
	while( fgets( nro_estacion, TAM, ( FILE* )nros_estaciones) != NULL )
	{
		nro_estacion_parseado = strtok( nro_estacion, "\n" );
		// Llamada a la función que envía el promedio de la variable solicitada, 
		// de todas las estaciones.
		enviar_titulo_promedio( nro_estacion_parseado, titulo );
    	memset( nro_estacion, '\0', sizeof( nro_estacion ));
	}

	fclose( nros_estaciones );

}
