#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAM 200

void concat_titulo_promedio( char* nro_estacion )
{
	char nro_estacion_copia[ TAM ];
    memset( nro_estacion_copia, '\0', sizeof( nro_estacion_copia ));

    strcpy( nro_estacion_copia, nro_estacion );
    FILE* titulos = fopen( strcat( nro_estacion_copia, "_titulos"), "r");

	memset( nro_estacion_copia, '\0', sizeof( nro_estacion_copia ));
	strcpy( nro_estacion_copia, nro_estacion );
    FILE* promedios = fopen( strcat( nro_estacion_copia, "_promedios"), "r");

	memset( nro_estacion_copia, '\0', sizeof( nro_estacion_copia ));
	strcpy( nro_estacion_copia, nro_estacion );
    FILE* titulos_promedios = fopen( strcat( nro_estacion_copia, "_titulos_promedios"), "w");

    char linea[ TAM ];
    memset( linea, '\0', sizeof( linea ));

    char* parseo;

    while( fgets( linea, TAM, ( FILE* ) titulos ) != NULL )
    {
    	if( strcmp( linea, "Direccion Viento\n" ))
    	{
    		parseo = strtok( linea, "[" );
    		fputs( linea, titulos_promedios );
    		fputs( ": ", titulos_promedios );
   		    memset( linea, '\0', sizeof( linea ));
   		    fgets( linea, TAM, ( FILE * ) promedios );
   		    fputs( linea, titulos_promedios );
			memset( linea, '\0', sizeof( linea ));
    	}
    }

    fclose( titulos );
    fclose( promedios );
    fclose( titulos_promedios );
}
