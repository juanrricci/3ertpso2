#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAM 256

int main ( )
{
        encabezado ( );

        printf ( "<h1>INSTALLER / ERASER DE MODULO</h1>\n" );

        char get_copia [ TAM ];
        memset ( get_copia, '\0', TAM );

        char* get = getenv ( "QUERY_STRING" );
        strcpy ( get_copia, get );

        char* parseo = strtok( get_copia, "=" );
	parseo = strtok ( NULL, "" );

	if ( !strcmp ( parseo, "instalar" ))
	{
		system ( "sudo insmod /var/www/html/cgi-bin/modulo.ko" );
		printf ( "<p>Modulo instalado correctamente</p>\n" );
	        printf ( "<p>Mensaje generado por el módulo:</p>\n" );

	        char buffer [ TAM ];
	        memset ( buffer, '\0', TAM );

        	FILE* mensaje = popen ( "dmesg | tail -1", "r" );

	        while ( fgets ( buffer, TAM, mensaje ) != NULL )
	        {
        	        printf ( "<p>%s</p>\n", buffer );
	        }
	
	}

        if ( !strcmp ( parseo, "eliminar" ))
        {
                system ( "sudo rm /var/www/html/cgi-bin/modulo.ko" );
                printf ( "<p>Modulo eliminado correctamente</p>\n" );
        }	

        pie ( );

        return 0;
}

