#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define TAM 256

int main ( )
{
        encabezado ( );

	system ( "sudo rmmod hello" );

        printf ( "<h1>UNINSTALLER DE MODULO</h1>\n" );

	printf ( "<p>Modulo desinstalado correctamente</p>\n" );
        printf ( "<p>Mensaje generado por el módulo:</p>\n" );

	char buffer [ TAM ];
        memset ( buffer, '\0', TAM );
	
	FILE* mensaje = popen ( "dmesg | tail -1", "r" );

	while ( fgets ( buffer, TAM, mensaje ) != NULL )
	{
		printf ( "<p>%s</p>\n", buffer );
	}

        pie ( );

        return 0;
}

