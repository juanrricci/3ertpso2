#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main( )
{
	encabezado( );
	printf( "<h1>DATOS METEOROLÓGICOS</h1>\n" );
	printf( "<h2>Descargar datos de una estación</h2>\n" );
	printf( "<form action=\"/cgi-bin/opcion_datos.cgi\" method=\"get\">\n"  ); 
		printf( "<input type=\"radio\" name=\"descargar\" value=\"30057\" checked> Estación 30057<br>\n" );
		printf( "<input type=\"radio\" name=\"descargar\" value=\"30061\"> Estación 30061<br>\n" );
		printf( "<input type=\"radio\" name=\"descargar\" value=\"30069\"> Estación 30069<br>\n" );
		printf( "<input type=\"radio\" name=\"descargar\" value=\"30099\"> Estación 30099<br>\n" );
		printf( "<input type=\"radio\" name=\"descargar\" value=\"30135\"> Estación 30135<br>\n" );
		printf( "<input type=\"submit\" value=\"Aceptar\">\n" );
	printf( "</form>\n" );
	printf( "<h2>Ver el acumulado diario de precipitaciones de una estación</h2>\n" );
	printf( "<form action=\"/cgi-bin/opcion_datos.cgi\" method=\"get\">\n"  ); 
		printf( "<input type=\"radio\" name=\"diario_precipitacion\" value=\"30057\" checked> Estación 30057<br>\n" );
		printf( "<input type=\"radio\" name=\"diario_precipitacion\" value=\"30061\"> Estación 30061<br>\n" );
		printf( "<input type=\"radio\" name=\"diario_precipitacion\" value=\"30069\"> Estación 30069<br>\n" );
		printf( "<input type=\"radio\" name=\"diario_precipitacion\" value=\"30099\"> Estación 30099<br>\n" );
		printf( "<input type=\"radio\" name=\"diario_precipitacion\" value=\"30135\"> Estación 30135<br>\n" );
		printf( "<input type=\"submit\" value=\"Aceptar\">\n" );
	printf( "</form>\n" );
	printf( "<h2>Ver el acumulado mensual de precipitaciones de una estación</h2>\n" );
	printf( "<form action=\"/cgi-bin/opcion_datos.cgi\" method=\"get\">\n"  ); 
		printf( "<input type=\"radio\" name=\"mensual_precipitacion\" value=\"30057\" checked> Estación 30057<br>\n" );
		printf( "<input type=\"radio\" name=\"mensual_precipitacion\" value=\"30061\"> Estación 30061<br>\n" );
		printf( "<input type=\"radio\" name=\"mensual_precipitacion\" value=\"30069\"> Estación 30069<br>\n" );
		printf( "<input type=\"radio\" name=\"mensual_precipitacion\" value=\"30099\"> Estación 30099<br>\n" );
		printf( "<input type=\"radio\" name=\"mensual_precipitacion\" value=\"30135\"> Estación 30135<br>\n" );
		printf( "<input type=\"submit\" value=\"Aceptar\">\n" );
	printf( "</form>\n" );
	printf( "<h2>Ver el promedio de una variable en todas las estaciones</h2>\n" );
	printf( "<form action=\"/cgi-bin/opcion_datos.cgi\" method=\"get\">\n"  ); 
		printf( "<input type=\"radio\" name=\"promedio\" value=\"Temperatura\" checked> Temperatura<br>\n" );
		printf( "<input type=\"radio\" name=\"promedio\" value=\"Humedad\"> Humedad<br>\n" );
		printf( "<input type=\"radio\" name=\"promedio\" value=\"Punto de Rocio\"> Punto de Rocío<br>\n" );
		printf( "<input type=\"radio\" name=\"promedio\" value=\"Precipitacion\"> Precipitación<br>\n" );
		printf( "<input type=\"radio\" name=\"promedio\" value=\"Velocidad Viento\"> Velocidad Viento<br>\n" );
		printf( "<input type=\"radio\" name=\"promedio\" value=\"Rafaga Maxima\"> Ráfaga Máxima<br>\n" );
		printf( "<input type=\"radio\" name=\"promedio\" value=\"Presion\"> Presión<br>\n" );
		printf( "<input type=\"radio\" name=\"promedio\" value=\"Radiacion Solar\"> Radiación Solar<br>\n" );
		printf( "<input type=\"radio\" name=\"promedio\" value=\"Temperatura Suelo 1\"> Temperatura Suelo 1<br>\n" );
		printf( "<input type=\"radio\" name=\"promedio\" value=\"Temperatura Suelo 2\"> Temperatura Suelo 2<br>\n" );
		printf( "<input type=\"radio\" name=\"promedio\" value=\"Temperatura Suelo 3\"> Temperatura Suelo 3<br>\n" );
		printf( "<input type=\"radio\" name=\"promedio\" value=\"Humedad Suelo 1\"> Humedad Suelo 1<br>\n" );
		printf( "<input type=\"radio\" name=\"promedio\" value=\"Humedad Suelo 2\"> Humedad Suelo 2<br>\n" );
		printf( "<input type=\"radio\" name=\"promedio\" value=\"Humedad Suelo 3\"> Humedad Suelo 3<br>\n" );
		printf( "<input type=\"radio\" name=\"promedio\" value=\"Humedad de Hoja\"> Humedad de Hoja<br>\n" );
		printf( "<input type=\"submit\" value=\"Aceptar\">\n" );
	printf( "</form>\n" );
	pie( );
	
	return 0;
}

