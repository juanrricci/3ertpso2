#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define TAM 200

void gen_archivo_prec( char* nro_estacion )
{
	// String donde se copia el numero de estacion ingresado.
	char nro_estacion_copia[ TAM ];
	memset( nro_estacion_copia, '\0', sizeof( nro_estacion_copia ));

	// String donde se almacena la linea extraida del archivo de datos.
	char linea[ TAM ];
	memset( linea,'\0', sizeof( linea ));
	
	// String donde se copia el dato parseado (fecha-hora y valor de precipitacion)
	char dato[ TAM ];
	memset( dato, '\0', sizeof( dato ));
	
	strcpy( nro_estacion_copia, nro_estacion );

	FILE* datos_estacion = fopen( nro_estacion, "r" );
	FILE* precipitaciones = fopen( strcat( nro_estacion_copia, "_precipitaciones" ), "w" );

	char *parseo;

	int contador_datos;
	int indicador_primera_linea = 1;

	while( fgets( linea, TAM, ( FILE* )datos_estacion) != NULL )
	{
		if( indicador_primera_linea > 1 )
		{
			fputs( "\n", precipitaciones );
		}

		indicador_primera_linea++;
		
		contador_datos = 1;
		parseo = strtok( linea, "," );
		strcpy( dato, parseo );

		while( strcmp( dato, "\n" ))
		{
			contador_datos++;
			parseo = strtok( NULL, "," );
			strcpy( dato, parseo );
			
			if( contador_datos == 4 )
			{
				// Se guarda fecha y hora de cada línea en el archivo de precipitaciones
				fputs( strcat( dato, "," ), precipitaciones );
			}
			
			else if( contador_datos == 8 )
			{
				// Se guarda el valor de precipitación de esa fecha y hora	
				fputs( dato, precipitaciones );
				memset( dato, '\0', sizeof( dato ));
				break;
			}
		}
	}

	fclose( precipitaciones );
	fclose( datos_estacion );
}
