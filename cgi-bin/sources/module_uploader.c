#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main ( )
{
        encabezado ( );

        printf ( "<h1>UPLOADER DE MODULO</h1>\n" );

	// 1) Se obtiene el tamaño del archivo subido desde la variable de entorno "CONTENT_LENGTH"

        char* tam_archivo = getenv ( "CONTENT_LENGTH" );
	
	// 2) Se castea el tamaño de archivo a entero

	int tam_archivo_int = atoi ( tam_archivo );

        printf ( "<p>Se subió un archivo de %d bytes</p>\n", tam_archivo_int );

	// 3) Se aloca memoria según el tamaño del archivo para poder guardarlo

	char* archivo_ram = ( char* ) malloc ( tam_archivo_int );

	if ( archivo_ram == NULL )
	{
		printf ( "<p>ERROR: no hay suficiente memoria RAM para subir el archivo</p>\n" );
	}

	else
	{
		printf ( "<p>La memoria fue alocada correctamente</p>\n" );
	}	

	// 4) Se lee el archivo recibido y se lo almacena en RAM.
	// Los parámetros de fread son:
	//  - "archivo_ram": es la porción de memoria alocada donde se almacena la información del archivo subido
	//  - "tam_archivo_int": es el tamaño en bytes del archivo subido
	//  - Número entero "1": especifica que toda la información se lee en un solo bloque
	//  - "stdin": file descriptor que apunta a la entrada estándar, que es donde se reciben los datos mediante POST.
	
	fread ( archivo_ram, tam_archivo_int, 1, stdin );	
	
	// 5) Se aloca un nuevo espacio de memoria donde se almacenará el modulo subido sin las cabeceras HTML

	char* archivo_limpio = ( char * ) malloc ( tam_archivo_int - 200 );

	// 6) Se itera sobre el espacio de memoria del archivo subido para copiar byte a byte la información
	// a otro espacio de memoria 
	int i;
	for ( i = 0; i < ( tam_archivo_int - 45 ); i++ )
	{
		*( archivo_limpio + i ) = *( archivo_ram + 154 + i );
	}

	// 7) Se almacenan los datos del módulo reconstruido en un nuevo archivo llamado "modulo.ko"
	
	FILE* modulo_bajado = fopen ( "modulo.ko", "wb" );	

	if ( !modulo_bajado )
	{
		printf("<p>No se puede abrir archivo de módulo</p>\n");
		return 1;
	}

	else
	{
                printf("<p>Archivo de módulo abierto correctamente</p>\n");
	}

	fwrite ( archivo_limpio, tam_archivo_int, 1, modulo_bajado );	

	// **** HASTA ACA ANDA BIEN, DESPUES YA NO (NO SE LOGRA CERRAR EL ARCHIVO) ****

	fclose ( modulo_bajado );

	// 8) Se libera el espacio alocado en RAM

	free ( archivo_ram );
	free ( archivo_limpio );

	// 9) Se cierra el archivo del módulo descargado

//	fclose ( modulo_bajado );

	// 10) Se ejecuta la instalación del archivo recibido: se muestra el mensaje de retorno
	// del sistema. Con ello, se sabe si el módulo se instaló correctamente o no.

//	system ( "sudo insmod /var/www/html/cgi-bin/modulo.ko" );

	pie ( );

        return 0;
}
