#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAM 200

void mensual_precipitacion( char *nro_estacion )
{
	gen_acum_mensual_prec( nro_estacion );

	char linea[ TAM ];
	memset( linea ,'\0', sizeof( linea ));

	char nro_estacion_copia[ TAM ];
	memset( nro_estacion_copia, '\0', sizeof( nro_estacion_copia ));

	strcpy( nro_estacion_copia, nro_estacion );
	FILE* acumulado_mensual = fopen( strcat( nro_estacion_copia, "_acum_mensual" ), "r" );


	while( fgets( linea, TAM,( FILE* )acumulado_mensual ) != NULL)
	{
		char* parseo = strtok( linea, "\n" );
		printf( "<p>%s</p>\n", parseo );
	}
}
