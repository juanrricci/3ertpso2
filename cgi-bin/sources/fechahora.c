#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAM 256

int fechahora( )
{
        char linea[ TAM ];
        memset( linea, '\0', TAM );

        printf( "<h1>FECHA Y HORA ACTUALES</h1>\n" );

        FILE* retorno = popen( "date", "r" );
        while( fgets( linea, TAM, ( FILE* ) retorno ) != NULL )
        {
                printf( "<p>%s</p>", linea );
        }
        pclose( retorno );

        return 0;
}

