#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAM 200

void enviar_titulo_promedio( char* nro_estacion, char* titulo )
{
	char linea[ TAM ];
    memset( linea, '\0', sizeof( linea ));

    char linea_copia[ TAM ];
    memset( linea_copia, '\0', sizeof( linea_copia ));

    char titulo_copia[ TAM ];
    memset( titulo_copia, '\0', sizeof( titulo_copia ));
    strcpy( titulo_copia, titulo );
    strcat( titulo_copia, " " );

	char nro_estacion_copia[ TAM ];
    memset( nro_estacion_copia, '\0', sizeof( nro_estacion_copia ));
    strcpy( nro_estacion_copia, nro_estacion );

    FILE* titulo_promedio = fopen( strcat( nro_estacion_copia, "_titulos_promedios"), "r" );
    char* parseo;
    int n;
    int acierto = 0;

    printf( "<p>%s</p>\n", nro_estacion );

    while( fgets( linea, TAM, ( FILE* ) titulo_promedio ) != NULL )
    {
    	strcpy( linea_copia, linea );
    	parseo = strtok( linea_copia, ":" );
    	if( !strcmp( parseo, titulo_copia ))
    	{
		printf( "<p>%s</p>\n", linea );
    		acierto = 1;
    	}
   	    memset( linea_copia, '\0', sizeof( linea_copia ));
    }

    fclose( titulo_promedio );

    if( !acierto )
    {
	printf( "<p>No se encontró la variable solicitada</p>\n" );
    }
}
